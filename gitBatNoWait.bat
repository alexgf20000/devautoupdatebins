REM Calculate parameter number
set argC=0
for %%x in (%*) do Set /A argC+=1
echo %argC%

REM Select message
set msg=Autoupdater commit
if %argC% gtr 0 set msg=%1

REM Dequote message
set msg=###%msg%###
set msg=%msg:"###=%
set msg=%msg:###"=%
set msg=%msg:###=%

REM Replace underscore by space
set msg=%msg:_= %

REM Git operations
cd C:\Alex\Freelancer\Repos\DevAutoupdateBins
git add .

REM git remote add origin https://programdelphi@bitbucket.org/compudime/autoupdatebins.git
REM git config --global user.email "UpdateMaster"
REM git config --global user.name "Update Master"

del output.txt
git commit -a -m"%msg%"
git push -u origin master > output.txt